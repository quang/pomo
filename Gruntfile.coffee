module.exports = (grunt)->
  grunt.initConfig
    express :
      dev :
        options :
          port : 3000
          script : 'server.coffee'
          opts : ['node_modules/coffee-script/bin/coffee']

    watch :
      html :
        files : 'public/index.html'

      coffee :
        files : 'src/**/*.coffee'
        tasks : ['browserify']

      options :
        spawn :false
        livereload : true
    
    browserify :
      dev :
        files :
          'public/app.js' : 'src/index.coffee'
        options :
          transform : ['coffeeify']
          browserifyOptions :
            extensions : '.coffee'


  grunt.loadNpmTasks 'grunt-express-server'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-browserify'

  ### Tasks ###

  grunt.registerTask 'dev', ['browserify', 'express', 'watch']

  grunt.registerTask 'default', ['dev']
