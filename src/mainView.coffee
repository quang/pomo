React = require 'react'
{div, button, audio, source} = React.DOM
_ = require 'underscore'
moment = require 'moment'

Timer = React.createFactory require './timer'
Goals = React.createFactory require './goals'

Pomo = require './model/pomo'
TodayPomos = require './collection/pomosToday'

###
  DataObject

  goals : (number) daily pomodoro goal
  pomos : [array] collection of successful pomodoros
    {
      source : 'timer'|'manual'
      endTime : (new Date).toISOString() format
    }
###

module.exports = React.createClass
  propTypes :
    timerStart : React.PropTypes.number  # could be null or timestamp
    startTimer : React.PropTypes.func
    stopTimer : React.PropTypes.func

    logout : React.PropTypes.func
    user : React.PropTypes.object
    Parse : React.PropTypes.object

  handleStartClick : (event)->
    event.preventDefault()
    startAudio = @refs.start.getDOMNode()
    startAudio.currentTime = 0
    startAudio.play()
    @props.startTimer()


  onTimerFinish : (startTime)->
    @props.stopTimer =>
      @refs.ding.getDOMNode().play()
      alert 'Success!'

      pomo = new Pomo()
      pomo.set
        endTime : moment(startTime).add(25, 'minutes').toDate()
        source : 'timer'

      pomo.setACL new @props.Parse.ACL @props.Parse.User.current()

      pomo.save
        success : (pomo)=>
          @state.pomosToday.add pomo
          @forceUpdate()

  resetTimer : ->
    @props.stopTimer @props.startTimer

  getInitialState : ->
    pomosToday : null

  componentWillMount : ->
    pomos = new TodayPomos
    pomos.fetch(
      success : (collection)=>
        @setState
          pomosToday : collection
    )

  render : ->
    els = []

    if @props.timerStart
      els.push Timer
        startTime : @props.timerStart
        onFinish : @onTimerFinish
        reset : @resetTimer
        stop : @props.stopTimer
    else
      els.push button {onClick:@handleStartClick, className: 'btn btn-success start-button'}, 'Start Pomodoro'

    els.push Goals
      logout : @props.logout
      user : @props.user
      pomos : @state.pomosToday

    els.push audio ref:'start',
      source src: '/start.ogg'

    els.push audio ref:'ding',
      source src: '/ding.wav'

    div.apply this, [id: 'main-view'].concat els
