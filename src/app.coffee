React = require 'react'
{div} = React.DOM

Front = React.createFactory require './front'
InApp = React.createFactory require './inApp'

module.exports  = React.createClass
  propTypes :
    Parse : React.PropTypes.object.isRequired

  getInitialState : ->

    state = {
      route : ''  # current list viewing
    }

    currentUser = @props.Parse.User.current()

    # If already logged in, set user state
    if currentUser
      state.user = currentUser

    else
      state.user = null

    # Route
    state.route = @getRoute()

    return state

  getRoute : ->
    # remove the beginning '#/'
    return location.hash.split('/').slice(1).join('/')

  login : (email, password, handleError)->
    @props.Parse.User.logIn email, password,
      success : (user)=>
        @setState
          user : user

      error : (user, error)->
        handleError error.message

  logout : ->
    @props.Parse.User.logOut()
    @setState user: null

  componentWillMount : ->
    $(window).on 'hashchange', =>
      @setState route : @getRoute()

  render : ->
    els = []

    if @state.user
      els.push InApp(
        user : @state.user
        route : @state.route
        logout : @logout
        Parse : @props.Parse
      )
    else
      els.push Front
        route : @state.route
        login : @login
        Parse : @props.Parse

    div.apply this, [null].concat els

