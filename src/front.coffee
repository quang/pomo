React = require 'react'
{div, h1, img} = React.DOM

TopNav = React.createFactory require './topNav'
Login = React.createFactory require './login'
Join = React.createFactory require './join'

module.exports = React.createClass
  propTypes :
    login : React.PropTypes.func
    route : React.PropTypes.string
    Parse : React.PropTypes.object

  render : ->
    els = []

    els.push TopNav(
      route : @props.route
    )

    els.push div className: 'col-sm-offset-2 col-sm-10 hidden-xs',
      h1 null,
        img className: 'logo-img', src:'/logo.png'
        'PomoTrackr'

    if @props.route is 'join'
      els.push Join
        login : @props.login
        Parse : @props.Parse
    else
      els.push Login
        login : @props.login
    
    div.apply this, [null].concat els
