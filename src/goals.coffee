React = require 'react'
{div, h1, a, span} = React.DOM
_ = require 'underscore'
$ = require 'jquery'
moment = require 'moment'

TodayPomos = require './collection/pomosToday'

Goal = React.createFactory require './goal'

DEFAULT_GOALS = 9

module.exports = React.createClass
  propTypes :
    pomos : React.PropTypes.object
    logout : React.PropTypes.func
    user : React.PropTypes.object

  getInitialState : ->

    return {
      goals : @props.user.get('goals') or DEFAULT_GOALS
    }

  handleCogClick : ->
    goals = prompt 'How many Pomodoros a day?', DEFAULT_GOALS
    if goals
      @setGoals goals

  setGoals : (goals)->
    goals = goals or DEFAULT_GOALS
    @props.user.save(goals:goals)
    @setState goals : goals

  startUpdateTimer : (options={})->
    clearTimeout @_updateTimer
    @forceUpdate() unless options.skip

    # Miliseconds to midnight
    msToMidnight = moment().endOf('day').diff moment()

    @_updateTimer = setTimeout @startUpdateTimer, msToMidnight

  render : ->
    els = []

    total = if @props.pomos then @props.pomos.length else 0

    _.times @state.goals, (count)=>
      count++
      els.push Goal
        done : count <= total
        total : parseInt @state.goals, 10
        count : count


    # For extra pomos
    if total > @state.goals
      _.times total - @state.goals, ->
        els.push Goal
          done : true
          extra : true

    els.push div className:'history-link',
      span {className : 'glyphicon glyphicon-cog', ref: 'cog'}
      a {href:'#/history'}, 'history'

    div.apply this, [{id : 'goals', title: total}].concat els


  componentDidMount : ->
    # Pomos clicking to group (add padding)
    $pomos = $(@getDOMNode()).children 'span'
    $pomos.on 'click', (event)->
      $(event.target).toggleClass 'pad-right'


  componentWillUpdate : ->
    $('.popover').remove()  # clean-up

  componentDidUpdate : ->
    # Cog
    cogEl = @refs.cog.getDOMNode()
    cogEl.react =
      handleCogClick : @handleCogClick
      logout : @props.logout

    $(cogEl).popover(
      content : '''
        <a href="#/about">about</a>
        <br>
        <a href="#" onClick="$(this).closest('.popover').prev()[0].react.handleCogClick()">settings</a>
        <br>
        <a href="#" onClick="$(this).closest('.popover').prev()[0].react.logout()">logout</a>
        '''
      html : true
      trigger : 'click'
    )

    # Update Daily
    @startUpdateTimer(skip:true)

