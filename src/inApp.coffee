React = require 'react'
{div, button} = React.DOM
$ = require 'jquery'

MainView = React.createFactory require './mainView'
History = React.createFactory require './history'
Day = React.createFactory require './day'
About = React.createFactory require './about'

module.exports = React.createClass
  propTypes :
    user : React.PropTypes.object
    logout : React.PropTypes.func
    route : React.PropTypes.string
    Parse : React.PropTypes.object

  mixins : [require 'reactfire']

  getInitialState : ->
    pomos : []
    timerStart : null

  startTimer : ->
    @setState timerStart : +new Date()

    @timer = setInterval =>
      @forceUpdate()
    , 1000

  stopTimer : (callback)->
    clearInterval @timer
    setTimeout =>
      @setState timerStart : null
      callback?()
    , 0

  render : ->
    if @props.route is 'history'
      return History()
    else if @props.route is 'about'
      return About()
    else if @props.route.indexOf('day/') isnt -1
      date = @props.route.split('/')[1]
      return Day
        date : date
        Parse : @props.Parse

    else
      return MainView
        timerStart : @state.timerStart
        startTimer : @startTimer
        stopTimer : @stopTimer
        logout : @props.logout
        user : @props.user
        Parse : @props.Parse
