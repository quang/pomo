React = require 'react'
{div, form, input, button, label} = React.DOM

module.exports = React.createClass
  propTypes : ->
    login : React.PropTypes.func.isRequired

  getInitialState : ->
    error : null

  handleSubmit : (event)->
    event.preventDefault()

    @resetErrorMsg()

    email = @refs.email.getDOMNode().value.trim()
    password = @refs.password.getDOMNode().value

    # Clear PW
    @refs.password.getDOMNode().value = ''

    @props.login email, password, @handleError

    return

  resetErrorMsg : ->
    @setState error : null

  handleError : (msg)->
    @setState error : msg

  render : ->
    els = []

    els.push div className:'bg-danger', @state.error

    els.push div className: 'form-group',
      label className:'col-sm-2 col-lg-offset-1 control-label', htmlFor:'loginEmailField', 'Email'
      div className: 'col-sm-5 col-lg-3',
        input {placeholder:'Enter Email', type:'email', ref:'email', className: 'form-control', id:'loginEmailField'}

    els.push div className: 'form-group',
      label className: 'col-sm-2 col-lg-offset-1 control-label', htmlFor:'loginPasswordField', 'Password'
      div className: 'col-sm-5 col-lg-3',
        input {placeholder:'Password', type:'password', ref:'password', className: 'form-control', id:'loginPasswordField'}

    els.push div className: 'form-group',
      div className : 'col-sm-offset-2 col-sm-10 col-lg-offset-3',
        button {type:'submit', className:'btn btn-primary'}, 'Login'

    form.apply this, [onSubmit : @handleSubmit, className: 'form-horizontal'].concat els
