React = require 'react'
{div, button, span} = React.DOM
moment = require 'moment'

module.exports = React.createClass
  propTypes :
    startTime : React.PropTypes.number.isRequired

    onFinish : React.PropTypes.func.isRequired
    reset : React.PropTypes.func.isRequired
    stop : React.PropTypes.func.isRequired

  handleResetClick : ->
    if confirm 'Reset Timer?'
      @props.reset()

  handleStopClick : ->
    if confirm 'Stop Pomo?'
      @props.stop()

  render : ->
    diff = Math.abs moment().diff @props.startTime

    pomodoro = moment.duration 25, 'minutes'
    #pomodoro = moment.duration 5, 'seconds'

    pomodoro.subtract diff

    if pomodoro.asSeconds() < 0
      @props.onFinish(@props.startTime)
      return null

    # Remainder
    min = pomodoro.minutes()
    sec = pomodoro.seconds().toString()
    sec = '0'+sec unless sec.length is 2  # pad seconds with leading zero if needed

    # Render
    els = []

    els.push div className : 'jumbotron',

      div id : 'timer', "#{min}:#{sec}"

      div className : 'btn-group',

        button {onClick:@handleResetClick, className: 'btn btn-default btn-xs', title: 'reset'},
          span className : 'glyphicon glyphicon-refresh'

        button {onClick: @handleStopClick, className: 'btn btn-default btn-xs', title: 'stop'},
          span className : 'glyphicon glyphicon-stop'



    div.apply this, [null].concat els
