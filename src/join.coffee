React = require 'react'
{div, form, input, button, label} = React.DOM

TopNav = React.createFactory require './topNav'

module.exports = React.createClass
  propTypes : ->
    login : React.PropTypes.func.isRequired
    Parse : React.PropTypes.object.isRequired

  getInitialState : ->
    error : null

  handleSubmit : (event)->
    event.preventDefault()

    @resetErrorMsg()

    email = @refs.email.getDOMNode().value.trim()
    password = @refs.password.getDOMNode().value
    password2 = @refs.password2.getDOMNode().value

    # Clear PW
    @refs.password.getDOMNode().value = ''
    @refs.password2.getDOMNode().value = ''


    # Verify
    if password isnt password2
      @handleError 'Please make sure the password matches.'
      return
    else if password.length < 6
      @handleError 'Password should be longer than 6 characters.'
      return

    @signUp email, password, @handleError

    return

  signUp : (email, password, errorHandler)->
    user = new @props.Parse.User()
    user.set 'username', email
    user.set 'email', email
    user.set 'password', password

    user.signUp null,
      success : (user)=>
        @props.login email, password, errorHandler
      error : (user, error)->
        errorHandler error.message

  resetErrorMsg : ->
    @setState error : null

  handleError : (msg)->
    @setState error : msg

  render : ->
    els = []

    els.push div className:'bg-danger', @state.error

    els.push div className: 'form-group',
      label className:'col-sm-2 col-lg-offset-1 control-label', htmlFor:'joinEmailField', 'Email'
      div className: 'col-sm-5 col-lg-3',
        input {placeholder:'Enter Email', type:'email', ref:'email', className: 'form-control', id:'joinEmailField'}

    els.push div className: 'form-group',
      label className: 'col-sm-2 col-lg-offset-1 control-label', htmlFor:'joinPasswordField', 'Password'
      div className: 'col-sm-5 col-lg-3',
        input {placeholder:'Password', type:'password', ref:'password', className: 'form-control', id:'joinPasswordField'}

    els.push div className: 'form-group',
      label className: 'col-sm-2 col-lg-offset-1 control-label', htmlFor:'joinVerifyPasswordField', 'Verify Password'
      div className: 'col-sm-5 col-lg-3',
        input {placeholder:'Verify Password', type:'password', ref:'password2', className: 'form-control', id:'joinVerifyPasswordField'}

    els.push div className: 'form-group',
      div className : 'col-sm-offset-2 col-sm-10 col-lg-offset-3',
        button {type:'submit', className:'btn btn-default'}, 'Sign Up'

    form.apply this, [onSubmit : @handleSubmit, className: 'form-horizontal'].concat els
