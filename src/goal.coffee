React = require 'react'
{span} = React.DOM

POMOS_IN_SET = 3

module.exports = React.createClass
  propTypes :
    done : React.PropTypes.bool
    extra : React.PropTypes.bool

    total : React.PropTypes.number
    count : React.PropTypes.number

  render : ->
    padRight = if @props.count % POMOS_IN_SET or @props.count is @props.total then '' else 'pad-right'

    if @props.done
      if @props.extra
        span className : 'glyphicon glyphicon-check extra'
      else
        span className : "glyphicon glyphicon-check #{padRight}"
    else
      span className : "glyphicon glyphicon-unchecked #{padRight}"
