exports.ds = 'v1'  # datastore scheme

# Env
if location.hostname is 'app.pomotrackr.com'
  exports.env = 'prod'
else
  exports.env = 'dev'
