React = require 'react'
{div, span} = React.DOM
_ = require 'underscore'
moment = require 'moment'

NUMBER_OF_WEEKS = 12
DEFAULT_GOALS = 9

module.exports = React.createClass
  propTypes :
    pomos : React.PropTypes.array
      
  render : ->
    els = []

    weeks = _.groupBy _.compact(@props.pomos), (pomo)->
      moment(pomo.endTime.iso).format('YYYY-ww')

    weekProps = []

    _.times NUMBER_OF_WEEKS, (num)->
      weekYear = moment().startOf('week')

      if num
        weekYear.subtract(num, 'week')

      weekYearKey = weekYear.format('YYYY-ww')


      weekProps.push
        weekNumber: weekYear.format 'w'
        pomoCount : weeks[weekYearKey]?.length or 0

    weekProps.reverse()

    # Find the "Height" of the graph.
    # or goal, whichever one is greater

    height = _.reduce weekProps, (memo, week)->
      if week.pomoCount > memo
        return week.pomoCount
      else
        return memo
    , DEFAULT_GOALS

    for week in weekProps
      els.push div null,
        div
          title : week.pomoCount
          style :
            height : week.pomoCount*1.5 + 'px'
        span null, week.weekNumber

    div.apply this, [{className: 'clearfix week-summary', style:{height: height*1.5+'px'}}].concat els
