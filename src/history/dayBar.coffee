React = require 'react'
{span, td, a} = React.DOM

module.exports = React.createClass
  propTypes :
    height : React.PropTypes.number
    day : React.PropTypes.object  # moment obj
    pomos : React.PropTypes.array

  render : ->
    cells = @props.pomos.length

    height = ((cells/@props.height) * 60) or 0

    style =
      height: height+'%'

    td null,
      span {className: 'day-bar', style: style}
      span null, cells
      a {href : "#/day/#{@props.day.format('YYYY-MM-DD')}"}, @props.day.format 'MMM D'
