React = require 'react'
{div, a, ul, li} = React.DOM
_ = require 'underscore'
moment = require 'moment'

HistoryPomos = require '../collection/pomosHistory'

Weeks = React.createFactory require './weeks'
Week = React.createFactory require './week'

NUMBER_OF_WEEKS = 6
DEFAULT_GOALS = 9

module.exports = React.createClass
  propTypes : {}

  getInitialState : ->
    pomos : null

  componentWillMount : ->
    pomos = new HistoryPomos
    pomos.fetch(
      success : (collection)=>
        @setState
          pomos : collection
    )

  render : ->
    pomos = if @state.pomos then @state.pomos.toJSON() else []

    els = []

    els.push a {href:'#/'}, 'back'

    els.push Weeks
      pomos : pomos
    
    weeks = _.groupBy _.compact(pomos), (pomo)->
      moment(pomo.endTime.iso).format('YYYY-ww')

    weekProps = []

    _.times NUMBER_OF_WEEKS, (num)->
      weekYear = moment().startOf('week')

      if num
        weekYear.subtract(num, 'week')

      weekYearKey = weekYear.format('YYYY-ww')


      weekProps.push
        startDayMoment: weekYear
        pomos : weeks[weekYearKey] or []


    # Find the "Height" of the graph.
    # Best Pomodoro day in last 6 weeks (used for height of week graph)
    # or goal, whichever one is greater
    weekAllPomos = _.reduce weekProps, (memo, week)->
      memo.concat week.pomos
    , []

    dayPomos = _.groupBy weekAllPomos, (pomo)->
      moment(pomo.endTime.iso).format 'L'

    height = _.reduce dayPomos, (memo, day)->
      if day.length > memo
        return day.length
      else
        return memo
    , DEFAULT_GOALS


    for week in weekProps
      els.push Week
        startDayMoment: week.startDayMoment
        pomos : week.pomos
        height : height


    div.apply this, [id:'history'].concat els
