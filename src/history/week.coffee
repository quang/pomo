React = require 'react'
{table, tr, th} = React.DOM
_ = require 'underscore'
moment = require 'moment'

DayBar = React.createFactory require './dayBar.coffee'

DAYS_IN_WEEK = 7

module.exports = React.createClass
  propTypes : ->
    startDayMoment : React.PropTypes.object
    pomos : React.PropTypes.array
    height : React.PropTypes.number

  render : ->
    weekTotal = @props.pomos.length

    els = []

    endDayMoment = @props.startDayMoment.clone().endOf 'week'
    els.push tr null,
      th {colSpan:7}, @props.startDayMoment.format('MMM D - ') + endDayMoment.format('MMM D #w')
    els.push tr null,
      th {colSpan:7}, "(#{weekTotal})"

    dayRowEls = []


    # Pomos
    days = _.groupBy _.compact(@props.pomos), (pomo)->
      moment(pomo.endTime.iso).format 'L'

    dayCount = @props.startDayMoment.clone()

    _.times DAYS_IN_WEEK, (num)=>
      dayRowEls.push DayBar
        day : dayCount.clone()
        height : @props.height
        pomos : days[dayCount.format 'L'] or []

      dayCount.add 1, 'day'

    els.push tr.apply this, [null].concat dayRowEls


    table.apply this, [className : 'week-graph'].concat els
