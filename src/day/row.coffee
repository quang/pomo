React = require 'react'
{li, span, button} = React.DOM
moment = require 'moment'

module.exports = React.createClass
  propTypes :
    id : React.PropTypes.string
    time : React.PropTypes.string
    manuallyCreatedAt : React.PropTypes.string
    source : React.PropTypes.string
    remove : React.PropTypes.func

  handleRemoveClick : ->
    if confirm 'Are you sure you want to delete this Pomodoro? ' + moment(@props.time).format 'hh:mma'
      @props.remove @props.id

  render : ->
    time = moment(@props.time)

    els = []


    if @props.source is 'timer'
      els.push span null, time.format 'hh:mma'
    else
      els.push null, 'manually - ' + moment(@props.manuallyCreatedAt).format('LLL')

    els.push button onClick : @handleRemoveClick, className : 'btn btn-link', 'remove'

    li.apply this, [null].concat els

