React = require 'react'
{div, h1, a, ol, button} = React.DOM
moment = require 'moment'
_ = require 'underscore'

Pomo = require '../model/pomo'
DayPomos = require '../collection/pomosDay'

Row = React.createFactory require './row'

module.exports = React.createClass
  propTypes :
    date : React.PropTypes.string

  getInitialState : ->
    pomos : null

  remove : (id)->
    @state.pomos.get(id).destroy
      success : => @forceUpdate()

  handleAddClick : ->
    endTime = moment(@props.date, 'YYYY-MM-DD').toDate()

    pomo = new Pomo()
    pomo.set
      endTime : endTime
      source : 'manual'

    pomo.setACL new @props.Parse.ACL @props.Parse.User.current()

    pomo.save
      success : (pomo)=>
        @state.pomos.add pomo
        @forceUpdate()


  componentWillMount : ->
    pomos = new (DayPomos.init @props.date)
    pomos.fetch(
      success : (collection)=>
        @setState
          pomos : collection
    )

  render : ->
    pomos = if @state.pomos then @state.pomos.toJSON() else []

    els = []

    day = moment(@props.date, 'YYYY-MM-DD')

    pomos = _.filter _.compact(pomos), (pomos)->
      moment(pomos.endTime.iso).format('L') is day.format 'L'

    pomos = _.sortBy pomos, (pomo)->
      moment(pomo.endTime.iso).unix()


    #Render

    els.push a {href : '#/history', className: 'nav'}, 'back'

    els.push h1 null, day.format 'LL'

    els.push button onClick : @handleAddClick, 'add'

    listEls = []

    for pomo in pomos
      continue unless pomo
      listEls.push Row
        id : pomo.objectId
        time : pomo.endTime.iso
        source : pomo.source
        remove : @remove
        manuallyCreatedAt : pomo.createdAt


    els.push ol.apply this, [null].concat listEls

    div.apply this, [id: 'dayView'].concat els
