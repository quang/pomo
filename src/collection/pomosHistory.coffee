# Last 12 Weeks of pomos
# TODO Parse only allow 1000 result limit, figure out a way around this.

Pomo = require '../model/pomo'
moment = require 'moment'

LIMIT = 1000

module.exports = class PomosHistory
  constructor : ->
    @collection = new Parse.Collection

  _getQuery : ->
    historyStart = moment().subtract(11, 'weeks').startOf('week').toDate()
    historyEnd = moment().endOf('week').toDate()
    (new Parse.Query(Pomo)).greaterThanOrEqualTo('endTime', historyStart).lessThanOrEqualTo('endTime', historyEnd).limit(LIMIT)

  fetch : (options, skip=0)->
    query = @_getQuery()
    query.skip(skip)
    query.find
      success : (pomos)=>
        @collection.add pomos
        if pomos.length is LIMIT
          @fetch(options, skip+LIMIT)
        else
          options.success? @collection

  toJSON : ->
    @collection.toJSON()
