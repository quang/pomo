Pomo = require '../model/pomo'
moment = require 'moment'

todayStart = moment().startOf('day').toDate()
todayEnd = moment().endOf('day').toDate()

module.exports = Parse.Collection.extend
  model: Pomo
  query: (new Parse.Query(Pomo)).greaterThanOrEqualTo('endTime', todayStart).lessThanOrEqualTo('endTime', todayEnd)
