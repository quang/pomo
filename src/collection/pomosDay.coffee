Pomo = require '../model/pomo'
moment = require 'moment'

todayStart = moment().startOf('day').toDate()
todayEnd = moment().endOf('day').toDate()

module.exports =
  init : (date)->
    dayStart = moment(date, 'YYYY-MM-DD').startOf('day').toDate()
    dayEnd = moment(date, 'YYYY-MM-DD').endOf('day').toDate()

    return Parse.Collection.extend
      model: Pomo
      query : (new Parse.Query(Pomo)).greaterThanOrEqualTo('endTime', dayStart).lessThanOrEqualTo('endTime', dayEnd)
