React = require 'react'
{div, a, h1, img, span, h5, br} = React.DOM

module.exports = React.createClass
  render: ->
    
    els = []

    els.push a {href:'#/'}, 'back'

    els.push div className: '',
      h1 null,
        img className: 'logo-img', src:'/logo.png'
        'PomoTrackr'

    els.push span null,
      'a '
      a href: 'http://potentfuture.com', 'PotentFuture.com'
      ' Product.'

    els.push br null
    els.push br null

    els.push div null,
      h5 null, 'Sounds'

      'Starting sound by '
      a href: 'https://www.freesound.org/people/Zabuhailo/', 'zabuhailo'

      br null

      'Finishing sound by '
      a href: 'https://www.freesound.org/people/qubodup/', 'qubodup'

    div.apply this, [id:'about'].concat els
