{env} = require './version'

if env is 'prod'  # Production Env
  appId = "0w0FNs2MIm5SLNrdNvYg1ZcQoU4L2HwpfyWm9ri4"
  jsKey = "RY8JZuktvU1zOLz5U1HBJXNK9wV54UxF0f0XrStY"
else  # Dev Env
  appId = "DgrngFVIouUJcI1sAM1MQiQdIiKh2hoWnKSKoam4"
  jsKey = "uXMTIUM9RIq6THLSSMZc8GnLx7snGOgNODXfV1Dl"

Parse.initialize(appId, jsKey)

React = require 'react'

App = React.createFactory require './app'

React.initializeTouchEvents true
React.render App(Parse:Parse), document.getElementById 'app'
