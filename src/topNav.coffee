React = require 'react'
{div, a, span} = React.DOM

module.exports = React.createClass
  propTypes :
    route : React.PropTypes.string

  render : ->
    if @props.route is 'join'
      aLink = a href: '#/', 'Login'
    else
      aLink = a href: '#/join', 'Join'

    div className : 'row', id : 'topNav',
      div className : 'col-sm-7 col-lg-6',
        span className : 'visible-xs-inline',
          a href: 'http://pomotrackr.com', 'PomoTrackr'
          ' | '
        span className : 'hidden-xs',
          a href: 'http://pomotrackr.com', 'Home'
          ' | '
          a href: 'http://pomotrackr.com/tour', 'Tour'
          ' | '
        aLink

