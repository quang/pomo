# pomo
Pomodoro Tracker

# Security

    {
        "rules": {
          "dev" : {
            "users" : {
              "$user_id" : {
                ".read" : "$user_id === auth.uid",
                ".write" : "$user_id === auth.uid"
              }
            }
          },
          "prod" : {
            "users" : {
              "$user_id" : {
                ".read" : "$user_id === auth.uid",
                ".write" : "$user_id === auth.uid"
              }
            }
          }
        }
    }
