path = require 'path'
express = require 'express'

app = express()

app.get '/', (req, res)->
  res.sendFile path.join __dirname, '/public/index.html'

# Static Files
app.use express.static __dirname + '/public'

port = Number(process.env.PORT or 5000)
app.listen port, ->
  console.log 'Listening on ' + port
